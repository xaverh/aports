# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Newbyte <newbyte@disroot.org>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Newbyte <newbyte@postmarketos.org>
pkgname=phosh
pkgver=0.37.1
pkgrel=0
pkgdesc="Wayland shell for GNOME on mobile devices"
# armhf: blocked by gnome-shell-schemas
# Blocked on s390x by gnome-session, gnome-settings-daemon, squeekboard and libhandy
# Blocked on ppc64le by gnome-session
arch="all !armhf !s390x !ppc64le"
url="https://gitlab.gnome.org/World/Phosh/phosh"
license="GPL-3.0-only"
triggers="$pkgname.trigger=/usr/lib/phosh/plugins/prefs"
depends="
	adwaita-icon-theme
	bash
	dbus-x11
	dbus:org.freedesktop.Secrets
	desktop-file-utils
	font-adobe-source-code-pro
	font-cantarell
	gnome-control-center
	gnome-session
	gnome-settings-daemon
	gnome-shell-schemas
	gsettings-desktop-schemas
	phoc
	phosh-keyboard
	xwayland
	"
makedepends="
	callaudiod-dev
	elogind-dev
	evince-dev
	evolution-data-server-dev
	feedbackd-dev
	gcr-dev
	gettext-dev
	glib-dev
	gnome-desktop-dev
	gtk+3.0-dev
	libadwaita-dev
	libgudev-dev
	libhandy1-dev
	libsecret-dev
	linux-pam-dev
	meson
	networkmanager-dev
	polkit-elogind-dev
	pulseaudio-dev
	py3-docutils
	upower-dev
	wayland-dev
	wayland-protocols
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dbg $pkgname-lang $pkgname-dev $pkgname-doc $pkgname-portalsconf"
source="https://download.gnome.org/sources/phosh/${pkgver%.*}/phosh-$pkgver.tar.xz
	phosh.desktop
	"

build() {
	# phoc tests need a running Wayland compositor
	abuild-meson \
		-Db_lto=true \
		-Dphoc_tests=disabled \
		-Dsystemd=false \
		-Dman=true \
		. output
	meson compile -C output
}

check() {
	xvfb-run -a meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir/" meson install --no-rebuild --skip-subprojects -C output

	install -D -m644 "$srcdir"/phosh.desktop \
		"$pkgdir"/usr/share/wayland-sessions/phosh.desktop
}

portalsconf() {
	install_if="$pkgname=$pkgver-r$pkgrel xdg-desktop-portal>=1.17.1"
	amove usr/share/xdg-desktop-portal/phosh-portals.conf
}

sha512sums="
8676ea965c0596403ea0c219da581bffd6b0f450f7a759f18e20602ef8b04f8442665ecc89755068a858401304d0229a961ed642088c2018288a9a19ca3e4a33  phosh-0.37.1.tar.xz
8ca4893a751311de326e198314669f5a276092ade99c6353c4c9ce070713fb1a5b1615e7fecb93b428dc79fd4001a9af43d24eafaf2545d7db464963fda25330  phosh.desktop
"
